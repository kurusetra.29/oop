<?php
require ('animal.php');
require_once ('Frog.php');
require_once ('Ape.php');
$sheep = new Animal("shaun");
// $sheep->set_name("Shaun");
// var_dump($sheep);
echo "$sheep->name<br>";
echo "$sheep->legs<br>";
echo "$sheep->cold_blooded<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "$sungokong->legs<br>";
echo "$sungokong->cold_blooded<br>";
$sungokong->yell();


$kodok = new Frog ("buduk");
echo "$kodok->legs<br>";
echo "$kodok->cold_blooded<br>";
$kodok->jump();
// echo "$kodok->legs<br>";
?>